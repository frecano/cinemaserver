package services

import com.google.inject.Inject
import dao.MovieDAO
import models.{Movie, MovieRegister, Reservation}
import scala.concurrent.{ExecutionContext, Future}

trait MovieService {
  def register(movie: MovieRegister): Future[Either[String, Unit]]
  def reserve(reservation: Reservation): Either[String, Unit]
  def retrieve(imdbId: String, screenId: String): Either[String, Movie]
}

class MovieServiceImpl @Inject()(implicit context: ExecutionContext, omdbWSConsumer: OmdbWSConsumer, movieDAO: MovieDAO)
  extends MovieService {

  override def register(movie: MovieRegister) = {
    getMovieTitle(movie.imdbId).map { title =>
      val newRegistry = registerToMovie(movie, title)
      movieDAO.create(newRegistry)
    }.recover {
      case _ => Left("There was an error attempting to figure out the title of the movie")
    }
  }

  override def reserve(reservation: Reservation) = {
    movieDAO.getMovie(reservation.imdbId, reservation.screenId) match {
      case Left(error) => Left(error)
      case Right(movie) =>
        if(hasEnoughSeats(movie))
          movieDAO.reserveASeat(movie)
        else
          Left("There are not enough seats available to reserve")

    }
  }

  override def retrieve(imdbId: String, screenId: String) = movieDAO.getMovie(imdbId, screenId)

  private def hasEnoughSeats(movie: Movie) = movie.reservedSeats - 1 >= 0

  private def getMovieTitle(imdbId: String) = {
    val responseFuture = omdbWSConsumer.callWebService(imdbId)
    responseFuture.map { response =>
      (response.json \ "Title").as[String]
    }
  }

  private def registerToMovie(movieRegister: MovieRegister, title: String) = {
    Movie(
      imdbId = movieRegister.imdbId,
      screenId = movieRegister.screenId,
      movieTitle = title,
      availableSeats = movieRegister.availableSeats,
      reservedSeats = movieRegister.availableSeats
    )
  }

}
