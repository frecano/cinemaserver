package services

import com.google.inject.Inject
import play.api.libs.ws.{WSClient, WSResponse}
import scala.concurrent.Future


trait OmdbWSConsumer {
  def callWebService(imdbId: String): Future[WSResponse]
}

class OmdbWSConsumerImpl @Inject()(ws: WSClient) extends OmdbWSConsumer {
  val omdbTitleRequest = ws.url("http://omdbapi.com/")
  def callWebService(imdbId: String) = omdbTitleRequest.withQueryString("i" -> imdbId).get()
}
