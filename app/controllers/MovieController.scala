package controllers

import com.google.inject.Inject
import controllers.CustomRequestHelper._
import models.{MovieRegister, Reservation}
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import scala.concurrent.{ExecutionContext, Future}
import services.MovieService


class MovieController @Inject()(implicit context: ExecutionContext, movieService: MovieService)
  extends Controller {

  def register = Action.async { request =>
    request.handleJsonRequest[MovieRegister] match {
      case Left(errorMessage) => Future.successful(BadRequest(errorMessage))
      case Right(movieRegister) =>
        movieService.register(movieRegister).map {
          case Right(_) => Ok("Record created successfully")
          case Left(error) => InternalServerError(error)
        }
    }
  }

  def reserve() = Action { request =>
    request.handleJsonRequest[Reservation] match {
      case Left(errorMessage) => BadRequest(errorMessage)
      case Right(reservation) =>
        movieService.reserve(reservation) match {
          case Left(error) => InternalServerError(error)
          case Right(_) => Ok("One seat was reserved successfully")
        }
    }
  }

  def retrieve(imdbId: String, screenId: String) = Action { request =>
    movieService.retrieve(imdbId, screenId)  match {
      case Right(movie) => Ok(Json.toJson(movie))
      case Left(error) => InternalServerError(error)
    }
  }
}
