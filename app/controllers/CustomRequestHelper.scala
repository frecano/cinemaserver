package controllers

import models.{Movie, MovieRegister, Reservation}
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}
import play.api.mvc.{AnyContent, Request}

object CustomRequestHelper {
  implicit val movieReads = Json.reads[MovieRegister]
  implicit val reservationReads = Json.reads[Reservation]
  implicit val movieWrites = Json.writes[Movie]

  implicit class RequestHelper(request: Request[AnyContent]) {
    def handleJsonRequest[T](implicit rds: Reads[T]): Either[String, T] = {
      request.body.asJson match {
        case Some(json) =>
          json.validate[T](rds) match {
            case success: JsSuccess[T] => Right(success.value)
            case _: JsError => Left("Invalid Json")
          }
        case None => Left("A Json body is required")
      }
    }
  }
}
