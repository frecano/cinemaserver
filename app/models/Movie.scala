package models

case class Movie(
  imdbId: String,
  screenId: String,
  movieTitle: String,
  availableSeats: Int,
  reservedSeats: Int)

case class MovieRegister(imdbId: String, availableSeats: Int, screenId: String)
case class Reservation(imdbId: String, screenId: String)