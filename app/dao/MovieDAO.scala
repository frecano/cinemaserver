package dao

import com.google.inject.Inject
import models.Movie
import play.api.db.{Database, _}

trait MovieDAO {
  def getMovie(imdbId: String, screenId: String): Either[String, Movie]
  def create(movie: Movie): Either[String, Unit]
  def reserveASeat(movie: Movie): Either[String, Unit]
}

class MovieDAOImpl @Inject()(@NamedDatabase("cinema") db: Database) extends MovieDAO{

  override def create(movie: Movie) = {
    try {
      db.withConnection { connection =>
        val statement = connection.createStatement
        statement.executeUpdate(getInsertStatement(movie))
        Right(())
      }
    } catch {
      case e: Exception =>  Left(e.getMessage)
    }
  }

  override def getMovie(imdbId: String, screenId: String) = {
    try {
      db.withConnection { connection =>
        val statement = connection.createStatement
        val result = statement.executeQuery(getQueryStatement(imdbId, screenId))
        if(result.next()) {
          Right(Movie(
            imdbId = result.getString("imdb_id"),
            screenId = result.getString("screen_id"),
            movieTitle = result.getString("movie_title"),
            availableSeats = result.getInt("available_seats"),
            reservedSeats = result.getInt("reserved_seats")
          ))
        } else {
          Left(s"No records found for imDB id $imdbId and screen id $screenId")
        }
      }
    } catch {
      case e: Exception =>  Left(e.toString)
    }
  }



  override def reserveASeat(movie: Movie) = {
    try {
      db.withConnection { connection =>
        val statement = connection.createStatement
        val result = statement.executeUpdate(
          getUpdateSeatesStatement(movie.imdbId, movie.screenId, movie.reservedSeats - 1))
        if(result == 1) Right(())
        else Left("No record updated. This should not happen")
      }
    } catch {
      case e: Exception =>  Left(e.getMessage)
    }
  }

  private def getInsertStatement(movie: Movie) = {
    s"INSERT INTO movie(imdb_id, screen_id, movie_title, available_seats, reserved_seats)" +
    s"VALUES ('${movie.imdbId}', '${movie.screenId}','${movie.movieTitle}', " +
    s"${movie.availableSeats}, ${movie.reservedSeats})"
  }

  private def getUpdateSeatesStatement(imdbId: String, screenId: String, seats: Int) = {
    s"UPDATE movie " +
    s"SET reserved_seats = $seats " +
    s"WHERE imdb_id = '$imdbId' and screen_id = '$screenId'"
  }

  private def getQueryStatement(imdbId: String, screenId: String) = {
    s"SELECT * FROM movie WHERE imdb_id = '$imdbId' and screen_id = '$screenId'"
  }

}
