package controllers

import models.Movie
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._
import scala.concurrent.Future
import services.MovieServiceImpl


class MovieControllerSpec extends PlaySpec with MockitoSugar {

  def getControllerWithMock() = {
    val movieServiceMock = mock[MovieServiceImpl]
    val controller = new MovieController()(defaultContext, movieServiceMock)
    (controller, movieServiceMock)
  }


  "Register movie" should {

    "reject a request without body" in {
      val (controller, movieServiceMock) = getControllerWithMock()
      val result = controller.register().apply(FakeRequest())

      status(result) mustBe BAD_REQUEST
      contentType(result) mustBe Some("text/plain")
      contentAsString(result) must include ("A Json body is required")
    }

    "reject a request with an invalid json" in {
      val (controller, movieServiceMock) = getControllerWithMock()
      val invalidJson = """{"imdbId": "tt0111161", "availableSeats": 100}"""
      val result = controller.register().apply(FakeRequest().withJsonBody(Json.parse(invalidJson)))

      status(result) mustBe BAD_REQUEST
      contentType(result) mustBe Some("text/plain")
      contentAsString(result) must include ("Invalid Json")
    }

    val validJsonRegister = Json.parse(
      """{
        "imdbId": "tt0111161",
        "availableSeats": 100,
        "screenId": "screen_123456"
        }""")

    "register a new movie" in {
      val (controller, movieServiceMock) = getControllerWithMock()
      when(movieServiceMock.register(any())).thenReturn(Future.successful(Right(())))
      val response = controller.register().apply(FakeRequest()
        .withJsonBody(validJsonRegister))

      contentAsString(response) must include ("Record created successfully")
      status(response) mustBe OK
      contentType(response) mustBe Some("text/plain")
    }

    "Escalate the error when the creation is unsuccessful" in {
      val errorMessage = "This is my escalated error"
      val (controller, movieServiceMock) = getControllerWithMock()
      when(movieServiceMock.register(any()))
        .thenReturn(Future.successful(Left(errorMessage)))
      val response = controller.register().apply(FakeRequest()
        .withJsonBody(validJsonRegister))

      contentAsString(response) must include (errorMessage)
      status(response) mustBe INTERNAL_SERVER_ERROR
      contentType(response) mustBe Some("text/plain")
    }
  }

  "Reserve a seat for a movie" should {
    "reject a request without body" in {
      val (controller, movieServiceMock) = getControllerWithMock()
      val result = controller.reserve().apply(FakeRequest())

      status(result) mustBe BAD_REQUEST
      contentType(result) mustBe Some("text/plain")
      contentAsString(result) must include ("A Json body is required")
    }

    val validJsonReserve = Json.parse(
      """{
        "imdbId": "tt0111161",
        "screenId": "screen_123456"
        }""")

    "Escalate the error when the reservation is unsuccessful" in {
      val errorMessage = "This is my escalated error"
      val (controller, movieServiceMock) = getControllerWithMock()
      when(movieServiceMock.reserve(any()))
        .thenReturn(Left(errorMessage))
      val response = controller.reserve().apply(FakeRequest()
        .withJsonBody(validJsonReserve))

      contentAsString(response) must include (errorMessage)
      status(response) mustBe INTERNAL_SERVER_ERROR
      contentType(response) mustBe Some("text/plain")
    }

    "reserver a seat in a movie" in {
      val (controller, movieServiceMock) = getControllerWithMock()
      when(movieServiceMock.reserve(any())).thenReturn(Right(()))
      val response = controller.reserve().apply(FakeRequest()
        .withJsonBody(validJsonReserve))

      contentAsString(response) must include ("One seat was reserved successfully")
      status(response) mustBe OK
      contentType(response) mustBe Some("text/plain")
    }
  }

  "Retrieve a movie information" should {
    "Escalate the error when there is an error in the server" in {
      val errorMessage = "This is my escalated error"
      val (controller, movieServiceMock) = getControllerWithMock()
      when(movieServiceMock.retrieve(anyString(), anyString()))
        .thenReturn(Left(errorMessage))
      val response = controller.retrieve("","").apply(FakeRequest())

      contentAsString(response) must include (errorMessage)
      status(response) mustBe INTERNAL_SERVER_ERROR
      contentType(response) mustBe Some("text/plain")
    }

    "Return a Json with the movie data" in {
      import controllers.CustomRequestHelper._
      val (controller, movieServiceMock) = getControllerWithMock()
      val movie = Movie("FakeImdbId", "FakeScreenId", "Fake Movie Title", 50, 25)
      when(movieServiceMock.retrieve(anyString(), anyString())).thenReturn(Right(movie))
      val response = controller.retrieve("","").apply(FakeRequest())

      contentAsString(response) must include (Json.toJson(movie).toString())
      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")
    }
  }
}
