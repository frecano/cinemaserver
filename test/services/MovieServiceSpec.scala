package services

import dao.MovieDAO
import models.{Movie, MovieRegister, Reservation}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.libs.ws.WSResponse
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class MovieServiceSpec extends PlaySpec with MockitoSugar {

  "Register a movie" should {
    "register a new movie" in {
      val register = MovieRegister("fakeImdbID", 25, "fakeScreen")
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val mockWSResponse = mock[WSResponse]
      when(mockWSResponse.json).thenReturn(Json.parse("""{"Title": "This is a fake title" }"""))
      when(omdbWsConsumer.callWebService(anyString())).thenReturn(Future.successful(mockWSResponse))
      val dao = mock[MovieDAO]
      when(dao.create(any())).thenReturn(Right(()))

      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)
      val result = Await.result(service.register(register), 5 seconds)
      result.isRight must be(true)
    }


    "Escalate the error there is a DB error" in {
      val errorMessage = "There is something wrong with the DB for testing"
      val register = MovieRegister("fakeImdbID", 25, "fakeScreen")
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val mockWSResponse = mock[WSResponse]
      when(mockWSResponse.json).thenReturn(Json.parse("""{"Title": "This is a fake title" }"""))
      when(omdbWsConsumer.callWebService(anyString())).thenReturn(Future.successful(mockWSResponse))
      val dao = mock[MovieDAO]
      when(dao.create(any())).thenReturn(Left(errorMessage))

      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)
      val result = Await.result(service.register(register), 5 seconds)
      result must matchPattern {
        case Left(errorMessage) =>
      }
    }

    "Return an error when the title cannot be fetch" in {
      val errorMessage = "There was an error attempting to figure out the title of the movie"
      val register = MovieRegister("fakeImdbID", 25, "fakeScreen")
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val mockWSResponse = mock[WSResponse]
      when(mockWSResponse.json).thenReturn(Json.parse("""{"itle": "This is a fake title" }"""))
      when(omdbWsConsumer.callWebService(anyString())).thenReturn(Future.successful(mockWSResponse))
      val dao = mock[MovieDAO]
      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)
      val result = Await.result(service.register(register), 5 seconds)
      result must matchPattern {
        case Left(errorMessage) =>
      }
    }
  }

  "Reserve a seat" should {
    "reserve a seat" in {
      val movie = Movie("FakeImdbId", "FakeScreenId", "Fake Movie Title", 50, 25)
      val reservation = Reservation("fakeImdbID", "fakeScreen")
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val dao = mock[MovieDAO]
      when(dao.reserveASeat(any())).thenReturn(Right(()))
      when(dao.getMovie(anyString(), anyString())).thenReturn(Right(movie))
      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)

      val result = service.reserve(reservation)
      result must matchPattern {
        case Right(()) =>
      }
    }

    "Escalate an error then there is something wrong with the DAO" in {
      val errorMessage = "There is something wrong"
      val reservation = Reservation("fakeImdbID", "fakeScreen")
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val dao = mock[MovieDAO]
      when(dao.reserveASeat(any())).thenReturn(Right(()))
      when(dao.getMovie(anyString(), anyString())).thenReturn(Left(errorMessage))
      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)

      val result = service.reserve(reservation)
      result must matchPattern {
        case Left(errorMessage) =>
      }
    }

    "Return an error when there are not enough seats to reserve" in {
      val errorMessage = "There are not enough seats available to reserve"
      val movie = Movie("FakeImdbId", "FakeScreenId", "Fake Movie Title", 50, 0)
      val reservation = Reservation("fakeImdbID", "fakeScreen")
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val dao = mock[MovieDAO]
      when(dao.reserveASeat(any())).thenReturn(Right(()))
      when(dao.getMovie(anyString(), anyString())).thenReturn(Right(movie))
      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)

      val result = service.reserve(reservation)
      result must matchPattern {
        case Left(errorMessage) =>
      }
    }
  }

  "Retrive movie data" should {
    "return a valid response" in {
      val movie = Movie("FakeImdbId", "FakeScreenId", "Fake Movie Title", 50, 25)
      val omdbWsConsumer = mock[OmdbWSConsumer]
      val dao = mock[MovieDAO]
      when(dao.getMovie(anyString(), anyString())).thenReturn(Right(movie))
      val service = new MovieServiceImpl()(defaultContext, omdbWsConsumer, dao)

      val result = service.retrieve("", "")
      result must matchPattern {
        case Right(Movie(_, _, _, _, _)) =>
      }
    }
  }

}
