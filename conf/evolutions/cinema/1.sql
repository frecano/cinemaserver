# Movies schema

# --- !Ups

CREATE TABLE Movie (
  imdb_id VARCHAR(255) NOT NULL,
  screen_id VARCHAR(255) NOT NULL,
  movie_title VARCHAR(255) NOT NULL,
  available_seats INTEGER NOT NULL,
  reserved_seats INTEGER NOT NULL,
  PRIMARY KEY (imdb_id, screen_id)
);

# --- !Downs

DROP TABLE Movie;
